/*
    Copyright (c) 2017 Pino Toscano <pino@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "newsgroupsmodel.h"

#include <QApplication>
#include <QSortFilterProxyModel>
#include <QTreeView>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QStringList args = QApplication::arguments();
    args.removeFirst();

    NewsgroupsModel model(NewsgroupsModel::TreeModel);
    if (args.count() > 0) {
        model.fillFromFile(args.at(0));
    }

    QSortFilterProxyModel proxy;
    proxy.setSourceModel(&model);
    QTreeView view;
    view.setModel(&proxy);
    view.setAlternatingRowColors(true);
    view.setHeaderHidden(true);
    view.sortByColumn(0, Qt::AscendingOrder);
    view.expandAll();
    view.show();

    return app.exec();
}
