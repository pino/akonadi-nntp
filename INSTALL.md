How to build
============

Requirements
------------

- CMake >= 2.8.12
- extra-cmake-modules >= 5.14.0
- Qt >= 5.4.0
    - core, dbus, gui, network, widgets
- KDE Frameworks >= 5.8.0
    - KConfig, KI18n, KItemViews, KIO, KTextWidgets, KWallet, KWidgetsAddons,
      KWindowSystem
    - KItemModels only if Qt < 5.10.0
- KMime >= 5.0
- Akonadi, AkonadiMime >= 5.0
- LibEtPan

Building
--------

See the general KDE instructions for building with CMake:
<https://community.kde.org/Guidelines_and_HOWTOs/CMake/Building>
