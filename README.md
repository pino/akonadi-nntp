Akonadi NNTP resource
=====================

What it is
----------

This is an Akonadi resource to show NNTP newsgroups as email messages.
This way it is possible to read them as messages in any Akonadi-based email
client (e.g. KMail).

Contact
-------

Please report bugs/questions/etc to Pino Toscano <pino@kde.org>.
