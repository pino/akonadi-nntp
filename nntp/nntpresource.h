/*
    Copyright (c) 2017 Pino Toscano <pino@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#ifndef NNTPRESOURCE_H
#define NNTPRESOURCE_H

#include <resourcebase.h>

#include <QScopedPointer>
#include <QUrl>

#include <libetpan/newsnntp.h>

class NewsgroupsModel;

class NntpResource : public Akonadi::ResourceBase, public Akonadi::AgentBase::Observer
{
    Q_OBJECT

public:
    explicit NntpResource(const QString &id);
    ~NntpResource();

public Q_SLOTS:
    void configure(WId windowId) Q_DECL_OVERRIDE;

protected Q_SLOTS:
    void retrieveCollections() Q_DECL_OVERRIDE;
    void retrieveItems(const Akonadi::Collection &col) Q_DECL_OVERRIDE;
    bool retrieveItems(const Akonadi::Item::List &items, const QSet<QByteArray> &parts) Q_DECL_OVERRIDE;
    bool retrieveItem(const Akonadi::Item &item, const QSet<QByteArray> &parts) Q_DECL_OVERRIDE;

protected:
    void collectionChanged(const Akonadi::Collection &collection) Q_DECL_OVERRIDE;
    void doSetOnline(bool online) Q_DECL_OVERRIDE;

private Q_SLOTS:
    void doRetrieveCollectionItems(const QString &password, bool userRejected);
    void doRetrieveItems(const QString &password, bool userRejected);
    void doRetrieveItem(const QString &password, bool userRejected);

private:
    QUrl baseRemoteId() const;

    bool doConnect();
    bool doAuthenticate(const QString &password);
    void closeConnection();
    bool switchToGroup(const QString &group);

    Akonadi::Collection findParentCollection(const QModelIndex &index);
    Akonadi::Collection createCollection(const QModelIndex &index) const;

private:
    Q_DISABLE_COPY(NntpResource)
    Akonadi::Collection::List mRemoteCollections;
    QScopedPointer<NewsgroupsModel> mGroups;
    QStringList mSubscribedGroups;
    newsnntp *mNntp;
    bool mAuthenticated;
    QString mNntpGroup;
    Akonadi::Item::List mFetchItems; // FIXME
};

#endif
