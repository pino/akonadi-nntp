/*
    Copyright (c) 2017 Pino Toscano <pino@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "nntputils.h"

#include <QSslSocket>

#include <klocalizedstring.h>

#include <libetpan/newsnntp.h>

#include <nntp_debug.h>

namespace NntpUtils
{

QString etpan_newsnntp_strerror(int code)
{
    switch (code) {
    case NEWSNNTP_NO_ERROR:
        // this shouldn't happen
        return QString();
    case NEWSNNTP_ERROR_REQUEST_AUTHORIZATION_USERNAME:
        return i18nc("EtPan error", "Username required");
    case NEWSNNTP_WARNING_REQUEST_AUTHORIZATION_PASSWORD:
        return i18nc("EtPan error", "Password required");
    case NEWSNNTP_ERROR_STREAM:
        return i18nc("EtPan error", "Stream error");
    case NEWSNNTP_ERROR_UNEXPECTED:
        return i18nc("EtPan error", "Unexpected error");
    case NEWSNNTP_ERROR_NO_NEWSGROUP_SELECTED:
        return i18nc("EtPan error", "No newsgroup selected");
    case NEWSNNTP_ERROR_NO_ARTICLE_SELECTED:
        return i18nc("EtPan error", "No article selected");
    case NEWSNNTP_ERROR_INVALID_ARTICLE_NUMBER:
        return i18nc("EtPan error", "Invalid article number");
    case NEWSNNTP_ERROR_ARTICLE_NOT_FOUND:
        return i18nc("EtPan error", "Article not found");
    case NEWSNNTP_ERROR_UNEXPECTED_RESPONSE:
        return i18nc("EtPan error", "Unexpected response");
    case NEWSNNTP_ERROR_INVALID_RESPONSE:
        return i18nc("EtPan error", "Invalid response");
    case NEWSNNTP_ERROR_NO_SUCH_NEWS_GROUP:
        return i18nc("EtPan error", "No such group");
    case NEWSNNTP_ERROR_POSTING_NOT_ALLOWED:
        return i18nc("EtPan error", "Posting now allowed");
    case NEWSNNTP_ERROR_POSTING_FAILED:
        return i18nc("EtPan error", "Posting failed");
    case NEWSNNTP_ERROR_PROGRAM_ERROR:
        return i18nc("EtPan error", "Program error");
    case NEWSNNTP_ERROR_NO_PERMISSION:
        return i18nc("EtPan error", "No permission");
    case NEWSNNTP_ERROR_COMMAND_NOT_UNDERSTOOD:
        return i18nc("EtPan error", "Command not understood");
    case NEWSNNTP_ERROR_COMMAND_NOT_SUPPORTED:
        return i18nc("EtPan error", "Command not supported");
    case NEWSNNTP_ERROR_CONNECTION_REFUSED:
        return i18nc("EtPan error", "Connection refused");
    case NEWSNNTP_ERROR_MEMORY:
        return i18nc("EtPan error", "Memory error");
    case NEWSNNTP_ERROR_AUTHENTICATION_REJECTED:
        return i18nc("EtPan error", "Authentication rejected");
    case NEWSNNTP_ERROR_BAD_STATE:
        return i18nc("EtPan error", "Bad state");
    case NEWSNNTP_ERROR_SSL:
        return i18nc("EtPan error", "SSL error");
    case NEWSNNTP_ERROR_AUTHENTICATION_OUT_OF_SEQUENCE:
        return i18nc("EtPan error", "Authentication out of sequence");
    }
    return i18nc("EtPan error", "Unknown EtPan code (%1)", code);
}

static ssize_t mailstream_low_qssl_read(mailstream_low *low, void *data, size_t len)
{
    QSslSocket *sock = static_cast<QSslSocket *>(low->data);
    if (sock->bytesAvailable() == 0) {
        const bool ready = sock->waitForReadyRead();
        if (!ready) {
            return -1;
        }
    }
    const qint64 actual = qMin<qint64>(len, sock->bytesAvailable());
    const qint64 ret = sock->read(static_cast<char *>(data), actual);
    return ret;
}

static ssize_t mailstream_low_qssl_write(mailstream_low *low, const void *data, size_t len)
{
    QSslSocket *sock = static_cast<QSslSocket *>(low->data);
    const qint64 ret = sock->write(static_cast<const char *>(data), len);
    return ret;
}

static int mailstream_low_qssl_close(mailstream_low *low)
{
    QSslSocket *sock = static_cast<QSslSocket *>(low->data);
    sock->disconnectFromHost();
    if (sock->state() != QAbstractSocket::UnconnectedState) {
        const bool disconnected = sock->waitForDisconnected();
        return disconnected ? 0 : -1;
    }
    return 0;
}

static int mailstream_low_qssl_get_fd(mailstream_low *low)
{
    QSslSocket *sock = static_cast<QSslSocket *>(low->data);
    return sock->socketDescriptor();
}

static void mailstream_low_qssl_free(mailstream_low *low)
{
    QSslSocket *sock = static_cast<QSslSocket *>(low->data);
    delete sock;
}

static mailstream_low_driver mailstream_low_qssl_driver {
    /* read */ mailstream_low_qssl_read,
    /* write */ mailstream_low_qssl_write,
    /* close */ mailstream_low_qssl_close,
    /* get_fd */ mailstream_low_qssl_get_fd,
    /* free */ mailstream_low_qssl_free,
    /* cancel */ nullptr,
    /* get_cancel */ nullptr,
    /* get_certificate_chain */ nullptr,
    /* setup_idle */ nullptr,
    /* unsetup_idle */ nullptr,
    /* interrupt_idle */ nullptr,
};

mailstream *mailstream_qsslsocket_new(QSslSocket *sock)
{
    mailstream_low *low = mailstream_low_new(sock, &mailstream_low_qssl_driver);
    mailstream *stream = mailstream_new(low, 8192);
    return stream;
}

int parse_nntp_reply(const QByteArray &reply)
{
    // the first three digits are checked when converting to numeric code
    if (reply.length() < 4 || reply.at(3) != ' ') {
        return NEWSNNTP_ERROR_UNEXPECTED_RESPONSE;
    }

    bool ok = false;
    const int code = QByteArray::fromRawData(reply.constData(), 3).toUInt(&ok, 10);
    if (!ok) {
        qCWarning(NNTP_LOG) << "no reply code in reply:" << reply;
        return NEWSNNTP_ERROR_UNEXPECTED_RESPONSE;
    }
    int ret;
    switch (code) {
    case 382:
        ret = NEWSNNTP_NO_ERROR;
        break;
    case 502:
        ret = NEWSNNTP_ERROR_NO_PERMISSION;
        break;
    case 580:
        ret = NEWSNNTP_ERROR_SSL;
        break;
    default:
        qCWarning(NNTP_LOG) << "unhandled reply code" << code << "for reply:" << reply;
        ret = NEWSNNTP_ERROR_UNEXPECTED_RESPONSE;
    }
    return ret;
}

}
