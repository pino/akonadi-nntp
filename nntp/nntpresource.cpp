/*
    Copyright (c) 2017 Pino Toscano <pino@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "nntpresource.h"

#include "configdialog.h"
#include "newsgroupsmodel.h"
#include "nntputils.h"
#include "settings.h"

#include <cachepolicy.h>
#include <changerecorder.h>
#include <collectionmodifyjob.h>
#include <entitydisplayattribute.h>
#include <Akonadi/KMime/MessageParts>

#include <kmime/kmime_message.h>
#include <kmime/kmime_util.h>

#include <kio/sslui.h>
#include <klocalizedstring.h>
#include <kwindowsystem.h>

#include <QSslSocket>

#include <algorithm>

extern "C" {
#include <libetpan/libetpan_version.h>
#include <libetpan/mailstream.h>
}

#include <nntp_debug.h>
#include <settingsadaptor.h>

using namespace Akonadi;
using namespace NntpUtils;

NntpResource::NntpResource(const QString &id)
    : ResourceBase(id)
    , mNntp(nullptr)
    , mAuthenticated(false)
{
    Settings::self()->setIdentifier(identifier());
    Settings::self()->setWinId(winIdForDialogs());

    changeRecorder()->fetchCollection(true);
    new SettingsAdaptor(Settings::self());
    QDBusConnection::sessionBus().registerObject(QStringLiteral("/Settings"),
            Settings::self(), QDBusConnection::ExportAdaptors);
    setNeedsNetwork(true);
    setDisableAutomaticItemDeliveryDone(true);

    mSubscribedGroups = Settings::self()->subscriptions();

    // needed to store Akonadi::Collection in QVariant, and thus
    // in NewsgroupsModel
    qRegisterMetaType<Akonadi::Collection>();
}

NntpResource::~NntpResource()
{
    closeConnection();
}

void NntpResource::configure(WId windowId)
{
    ConfigDialog dlg;
    if (windowId) {
        KWindowSystem::setMainWindow(&dlg, windowId);
    }
    if (!dlg.exec()) {
        emit configurationDialogRejected();
        return;
    }

    if (!Settings::self()->name().isEmpty()) {
        setName(Settings::self()->name());
    }
    mSubscribedGroups = Settings::self()->subscriptions();
    synchronizeCollectionTree();

    emit configurationDialogAccepted();
}

void NntpResource::retrieveCollections()
{
    mGroups.reset();
    mRemoteCollections.clear();
    mRemoteCollections.reserve(mSubscribedGroups.count() + 1);
    Collection root;
    root.setParentCollection(Collection::root());
    root.setRemoteId(baseRemoteId().url());
    root.setName(Settings::self()->name());
    static const QStringList contentTypes = QStringList()
        << Collection::mimeType()
    ;
    root.setContentMimeTypes(contentTypes);
    root.setRights(Collection::ReadOnly);
    CachePolicy policy;
    policy.setInheritFromParent(false);
    policy.setSyncOnDemand(true);
    QStringList localParts = QStringList()
        << QLatin1Literal(Akonadi::MessagePart::Envelope)
        << QLatin1Literal(Akonadi::MessagePart::Header);
    int cacheTimeout = 60;
    if (Settings::self()->disconnectedModeEnabled()) {
        // For disconnected mode we also cache the body
        // and we keep all data indefinitely
        localParts << QLatin1Literal(Akonadi::MessagePart::Body);
        cacheTimeout = -1;
    }
    policy.setLocalParts(localParts);
    policy.setCacheTimeout(cacheTimeout);
    policy.setIntervalCheckTime(Settings::self()->intervalCheckEnabled() ? Settings::self()->intervalCheckTime() : -1);
    root.setCachePolicy(policy);
    mRemoteCollections << root;

    mGroups.reset(new NewsgroupsModel(NewsgroupsModel::FlatModel));

    if (!doConnect()) {
        return;
    }

    foreach (const QString &name, Settings::self()->subscriptions()) {
        const QModelIndex index = mGroups->addNewsgroup(name);
        Q_ASSERT(index.isValid());
        Collection coll = createCollection(index);
        coll.setParentCollection(findParentCollection(index));
        mGroups->setData(index, QVariant::fromValue(coll), NewsgroupsModel::CollectionRole);
        mRemoteCollections << coll;
    }

    collectionsRetrieved(mRemoteCollections);
}

void NntpResource::retrieveItems(const Akonadi::Collection &col)
{
    if (col.remoteId() == baseRemoteId().url()) {
        itemsRetrievalDone();
        return;
    }

    if (Settings::self()->requiresAuthentication()) {
        connect(Settings::self(), &Settings::passwordRequestCompleted, this, &NntpResource::doRetrieveCollectionItems);
        Settings::self()->requestPassword();
    } else {
        QMetaObject::invokeMethod(this, "doRetrieveCollectionItems", Qt::QueuedConnection, Q_ARG(QString, QString()), Q_ARG(bool, false));
    }
}

void NntpResource::doRetrieveCollectionItems(const QString &password, bool userRejected)
{
    disconnect(Settings::self(), &Settings::passwordRequestCompleted, this, &NntpResource::doRetrieveCollectionItems);

    if (userRejected) {
        cancelTask(i18n("Could not read the password: user rejected wallet access"));
        return;
    }

    if (!doConnect() || !doAuthenticate(password)) {
        return;
    }

    setItemStreamingEnabled(true);

    Collection col = currentCollection();
    const QString groupName = col.remoteId();

    emit status(Running, i18nc("@info:status", "Switching to group '%1'.", groupName));
    emit percent(0);
    struct newsnntp_group_info *info = nullptr;
    int ret = newsnntp_group(mNntp, groupName.toUtf8().constData(), &info);
    qCDebug(NNTP_LOG) << name() << "group:" << ret << info;
    if (ret != NEWSNNTP_NO_ERROR) {
        cancelTask(i18n("Error while switching to group '%1': %2", groupName, etpan_newsnntp_strerror(ret)));
        return;
    }
    qCDebug(NNTP_LOG).nospace() << name() << " group info: [" << info->grp_first << "," << info->grp_last << "] count=" << info->grp_count;
    mNntpGroup = groupName;

    const QString revision = col.remoteRevision();
    const uint32_t first = !revision.isEmpty()
                         ? revision.toUInt() + 1
                         : info->grp_first;
    const int count = std::min(std::min(Settings::self()->maxDownload(),
                                        info->grp_last - first + 1),
                               info->grp_count);
    qCDebug(NNTP_LOG).nospace() << name() << " retrieve " << groupName
        << ": range=[" << first << "," << info->grp_last << "]; calculated count: "
        << Settings::self()->maxDownload() << "," << revision << ","
        << (info->grp_last - first + 1) << " => " << count;

    const QUrl base = count > 0 ? baseRemoteId() : QUrl();

    const QStringList localParts = col.cachePolicy().localParts();
    const bool onlyHeader = !localParts.contains(QLatin1String(Akonadi::MessagePart::Body));
    qCDebug(NNTP_LOG) << name() << "cache policy local parts:" << localParts;

    emit status(Running, i18nc("@info:status", "Fetching articles in '%1'.", groupName));
    Item::List newItems;
    newItems.reserve(count);
    for (uint32_t id = info->grp_last; id >= first && id > 0 && newItems.count() < count; --id) {
//         qCDebug(NNTP_LOG) << name() << "id:" << id;
        emit percent(newItems.count() * 100 / count);
        char *result = nullptr;
        size_t result_len = 0;
        ret = onlyHeader
            ? newsnntp_head(mNntp, id, &result, &result_len)
            : newsnntp_article(mNntp, id, &result, &result_len);
//         qCDebug(NNTP_LOG) << name() << "head/article:" << ret << result_len;
        if (ret != NEWSNNTP_NO_ERROR) {
            if (ret == NEWSNNTP_ERROR_INVALID_ARTICLE_NUMBER || ret == NEWSNNTP_ERROR_ARTICLE_NOT_FOUND) {
                continue;
            }
            qCWarning(NNTP_LOG) << "newsnntp_head/article for" << id << "in" << groupName << "failed: error =" << ret << "-" << etpan_newsnntp_strerror(ret);
            continue;
        }

        QUrl url = base;
        url.setPath(QStringLiteral("/") + groupName + QLatin1Char('/') + QString::number(id));
        Item item;
        item.setRemoteId(url.url());
        item.setMimeType(KMime::Message::mimeType());

        KMime::Message *msg = new KMime::Message();
        if (onlyHeader) {
            msg->setHead(KMime::CRLFtoLF(QByteArray::fromRawData(result, result_len)));
        } else {
            msg->setContent(KMime::CRLFtoLF(QByteArray::fromRawData(result, result_len)));
        }
        msg->parse();
        item.setPayload(KMime::Message::Ptr(msg));
        newItems.append(item);

        if (onlyHeader) {
            newsnntp_head_free(result);
        } else {
            newsnntp_article_free(result);
        }
    }

    itemsRetrievedIncremental(newItems, Item::List());
    itemsRetrievalDone();

    if (newItems.count() > 0) {
        // store last serial number
        col.setRemoteRevision(QString::number(info->grp_last));
        CollectionModifyJob *modify = new CollectionModifyJob(col);
        // TODO: check result signal
        Q_UNUSED(modify)
    }

    newsnntp_group_free(info);
}

bool NntpResource::retrieveItems(const Akonadi::Item::List &items, const QSet<QByteArray> &parts)
{
    Q_UNUSED(parts);
    // XXX apparently currentItems() returns items with no remote id
    // when called later on in doRetrieveItems(), so cache them here
    mFetchItems = items;

    if (Settings::self()->requiresAuthentication()) {
        connect(Settings::self(), &Settings::passwordRequestCompleted, this, &NntpResource::doRetrieveItems);
        Settings::self()->requestPassword();
    } else {
        QMetaObject::invokeMethod(this, "doRetrieveItems", Qt::QueuedConnection, Q_ARG(QString, QString()), Q_ARG(bool, false));
    }

    return true;
}

void NntpResource::doRetrieveItems(const QString &password, bool userRejected)
{
    disconnect(Settings::self(), &Settings::passwordRequestCompleted, this, &NntpResource::doRetrieveItem);

    if (userRejected) {
        cancelTask(i18n("Could not read the password: user rejected wallet access"));
        return;
    }

    if (!doConnect() || !doAuthenticate(password)) {
        return;
    }

    Item::List items = mFetchItems;
    mFetchItems.clear();
    if (items.isEmpty()) {
        return;
    }

    // they are all from the same collection, so all parts of the
    // same group
    const QString groupName = QUrl(items.at(0).remoteId()).path().section(QLatin1Char('/'), 0, 0, QString::SectionSkipEmpty);
    qCDebug(NNTP_LOG) << name() << "fetching" << items.count() << "items of group" << groupName;

    emit status(Running, i18nc("@info:status", "Switching to group '%1'.", groupName));
    emit percent(0);
    if (!switchToGroup(groupName)) {
        return;
    }

    for (int i = 0; i < items.count(); ++i) {
        Item item = items.at(i);
        const QUrl itemUrl(item.remoteId());
        const uint32_t indx = itemUrl.fileName().toUInt();

        char *result = nullptr;
        size_t result_len = 0;
        emit status(Running, i18nc("@info:status", "Fetching article %1 in '%2'.", indx, groupName));
        emit percent(i * 100 / items.count());
        int ret = newsnntp_article(mNntp, indx, &result, &result_len);
        qCDebug(NNTP_LOG) << name() << "article:" << ret << result_len;
        if (ret != NEWSNNTP_NO_ERROR) {
            cancelTask(i18n("Error while fetching the article: %1", etpan_newsnntp_strerror(ret)));
            return;
        }

        KMime::Message *msg = new KMime::Message();
        msg->setContent(KMime::CRLFtoLF(QByteArray::fromRawData(result, result_len)));
        msg->parse();
        item.setMimeType(KMime::Message::mimeType());
        item.setPayload(KMime::Message::Ptr(msg));
        newsnntp_article_free(result);
        items[i] = item;
    }

    itemsRetrieved(items);
}

bool NntpResource::retrieveItem(const Akonadi::Item &item, const QSet<QByteArray> &parts)
{
    Q_UNUSED(item);
    Q_UNUSED(parts);

    if (Settings::self()->requiresAuthentication()) {
        connect(Settings::self(), &Settings::passwordRequestCompleted, this, &NntpResource::doRetrieveItem);
        Settings::self()->requestPassword();
    } else {
        QMetaObject::invokeMethod(this, "doRetrieveItem", Qt::QueuedConnection, Q_ARG(QString, QString()), Q_ARG(bool, false));
    }

    return true;
}

void NntpResource::doRetrieveItem(const QString &password, bool userRejected)
{
    disconnect(Settings::self(), &Settings::passwordRequestCompleted, this, &NntpResource::doRetrieveItem);

    if (userRejected) {
        cancelTask(i18n("Could not read the password: user rejected wallet access"));
        return;
    }

    if (!doConnect() || !doAuthenticate(password)) {
        return;
    }

    Item item = currentItem();
    const QUrl itemUrl(item.remoteId());

    const QString groupName = itemUrl.path().section(QLatin1Char('/'), 0, 0, QString::SectionSkipEmpty);
    qCDebug(NNTP_LOG) << name() << "fetching item" << itemUrl.fileName() << "of group" << groupName;

    emit status(Running, i18nc("@info:status", "Switching to group '%1'.", groupName));
    emit percent(0);
    if (!switchToGroup(groupName)) {
        return;
    }

    const uint32_t indx = itemUrl.fileName().toUInt();

    char *result = nullptr;
    size_t result_len = 0;
    emit status(Running, i18nc("@info:status", "Fetching article %1 in '%2'.", indx, groupName));
    emit percent(50);
    int ret = newsnntp_article(mNntp, indx, &result, &result_len);
    qCDebug(NNTP_LOG) << name() << "article:" << ret << result_len;
    if (ret != NEWSNNTP_NO_ERROR) {
        cancelTask(i18n("Error while fetching the article: %1", etpan_newsnntp_strerror(ret)));
        return;
    }

    KMime::Message *msg = new KMime::Message();
    msg->setContent(KMime::CRLFtoLF(QByteArray::fromRawData(result, result_len)));
    msg->parse();
    item.setMimeType(KMime::Message::mimeType());
    item.setPayload(KMime::Message::Ptr(msg));
    itemRetrieved(item);

    newsnntp_article_free(result);
}

void NntpResource::collectionChanged(const Akonadi::Collection &collection)
{
    if (collection.remoteId() == baseRemoteId().url() && !collection.name().isEmpty()) {
        Settings::self()->setName(collection.name());
        setName(collection.name());
    }
    changeCommitted(collection);
}

void NntpResource::doSetOnline(bool online)
{
    ResourceBase::doSetOnline(online);
    if (!online) {
        closeConnection();
    }
}

QUrl NntpResource::baseRemoteId() const
{
    QUrl url;
    url.setScheme(QStringLiteral("nntp"));
    url.setHost(Settings::self()->server());
    if (Settings::self()->requiresAuthentication()) {
        url.setUserName(Settings::self()->userName());
    }
    return url;
}

bool NntpResource::doConnect()
{
    qCDebug(NNTP_LOG) << name() << "newsnntp =" << mNntp;
    if (mNntp) {
        return true;
    }

    qCDebug(NNTP_LOG).nospace() << "EtPan version: " << libetpan_get_version_major() << "." << libetpan_get_version_minor();

    // easy cleanup before the mailstream is created
    QScopedPointer<QSslSocket> sockPtr(new QSslSocket());
    QSslSocket *sock = sockPtr.data();
    sock->setSocketOption(QAbstractSocket::KeepAliveOption, 1);
    connect(sock, static_cast<void (QSslSocket::*)(const QList<QSslError> &)>(&QSslSocket::sslErrors),
            this, [&](const QList<QSslError> &errors) {
                bool ignored = KIO::SslUi::askIgnoreSslErrors(sock, KIO::SslUi::RecallAndStoreRules);
                qCDebug(NNTP_LOG) << name() << "socket SSL errors:" << errors << ignored;
                if (ignored) {
                    sock->ignoreSslErrors(errors);
                }
            });
    if (Settings::self()->encryption() == Settings::EncryptionSsl) {
        sock->connectToHostEncrypted(Settings::self()->server(), Settings::self()->port());
    } else {
        sock->connectToHost(Settings::self()->server(), Settings::self()->port());
    }
    if (!sock->waitForConnected()) {
        cancelTask(i18n("Error while connecting to %1:%2: %3", Settings::self()->server(), Settings::self()->port(), sock->errorString()));
        return false;
    }

    // construct the new mailstream, giving it the ownership of the socket
    mailstream *stream = mailstream_qsslsocket_new(sockPtr.take());
    mNntp = newsnntp_new(0, nullptr);
    int ret = newsnntp_connect(mNntp, stream);
    qCDebug(NNTP_LOG) << name() << "connect:" << ret;
    if (ret != NEWSNNTP_NO_ERROR) {
        cancelTask(i18n("Error while connecting to %1:%2: %3", Settings::self()->server(), Settings::self()->port(), etpan_newsnntp_strerror(ret)));
        closeConnection();
        return false;
    }

    if (Settings::self()->encryption() == Settings::EncryptionTls) {
        sock->write("STARTTLS\r\n");
        if (!sock->waitForReadyRead()) {
            qCDebug(NNTP_LOG) << name() << "socket state TLS:" << sock->state();
            closeConnection();
            return false;
        }
        ret = parse_nntp_reply(sock->readAll());
        if (ret != NEWSNNTP_NO_ERROR) {
            qCDebug(NNTP_LOG) << name() << "TLS error" << ret;
            cancelTask(i18n("Error while starting TLS encryption: %1", etpan_newsnntp_strerror(ret)));
            closeConnection();
            return false;
        }
        sock->startClientEncryption();
    }

    if (Settings::self()->encryption() != Settings::EncryptionNone) {
        if (!sock->waitForEncrypted()) {
            qCDebug(NNTP_LOG) << name() << "socket state TLS:" << sock->state();
            closeConnection();
            return false;
        }
        if (!sock->isEncrypted()) {
            cancelTask(i18n("The connection is not encrypted, while was requested"));
            closeConnection();
            return false;
        }
    }

    ret = newsnntp_mode_reader(mNntp);
    // TODO: not in RFC 977, so we should not abort here
    qCDebug(NNTP_LOG) << name() << "reader:" << ret;
    if (ret != NEWSNNTP_NO_ERROR && ret != NEWSNNTP_ERROR_COMMAND_NOT_UNDERSTOOD && ret != NEWSNNTP_ERROR_COMMAND_NOT_SUPPORTED) {
        cancelTask(i18n("Error when setting reader mode: %1", etpan_newsnntp_strerror(ret)));
        closeConnection();
        return false;
    }

    // no authentication required, so mark it as done
    if (!Settings::self()->requiresAuthentication()) {
        mAuthenticated = true;
    }

    connect(sock, &QSslSocket::disconnected,
            this, [&, sock]() {
                qCDebug(NNTP_LOG) << name() << "disconnected:" << sock->error() << sock->errorString();
                closeConnection();
            }, Qt::QueuedConnection);

    return true;
}

bool NntpResource::doAuthenticate(const QString &password)
{
    Q_ASSERT(mNntp);

    if (mAuthenticated) {
        return true;
    }

    Q_ASSERT(Settings::self()->requiresAuthentication());

    int ret = newsnntp_authinfo_username(mNntp, Settings::self()->userName().toUtf8().constData());
    qCDebug(NNTP_LOG) << name() << "authinfo_username:" << ret;
    if (ret == NEWSNNTP_WARNING_REQUEST_AUTHORIZATION_PASSWORD) {
        ret = newsnntp_authinfo_password(mNntp, password.toUtf8().constData());
        qCDebug(NNTP_LOG) << name() << "authinfo_password:" << ret;
        if (ret != NEWSNNTP_NO_ERROR) {
            cancelTask(i18n("Error when setting password for %1: %2", Settings::self()->userName(), etpan_newsnntp_strerror(ret)));
            return false;
        }
    } else if (ret != NEWSNNTP_NO_ERROR) {
        cancelTask(i18n("Error when setting username: %1", etpan_newsnntp_strerror(ret)));
        return false;
    }

    mAuthenticated = true;

    return true;
}

void NntpResource::closeConnection()
{
    if (!mNntp) {
        return;
    }

    newsnntp_quit(mNntp);
    newsnntp_free(mNntp);
    mNntp = nullptr;
    mAuthenticated = false;
    mNntpGroup.clear();
}

bool NntpResource::switchToGroup(const QString &group)
{
    qCDebug(NNTP_LOG) << name() << "from" << mNntpGroup << "to" << group;
    if (group.isEmpty()) {
        cancelTask(i18n("Trying to switch to an empty group."));
        return false;
    }
    if (mNntpGroup == group) {
        return true;
    }

    struct newsnntp_group_info *info = nullptr;
    int ret = newsnntp_group(mNntp, group.toUtf8().constData(), &info);
    qCDebug(NNTP_LOG) << name() << "group:" << ret << info;
    if (ret != NEWSNNTP_NO_ERROR) {
        cancelTask(i18n("Error while switching to group '%1': %2", group, etpan_newsnntp_strerror(ret)));
        return false;
    }

    newsnntp_group_free(info);
    mNntpGroup = group;

    return true;
}

Akonadi::Collection NntpResource::findParentCollection(const QModelIndex &index)
{
    const QModelIndex parent = index.parent();
    if (!parent.isValid()) {
        return mRemoteCollections.first();
    }
    const QVariant value = parent.data(NewsgroupsModel::CollectionRole);
    if (value.isValid()) {
        return value.value<Akonadi::Collection>();
    }
    Akonadi::Collection coll = createCollection(parent);
    coll.setParentCollection(findParentCollection(parent));
    mGroups->setData(parent, QVariant::fromValue(coll), NewsgroupsModel::CollectionRole);
    mRemoteCollections << coll;
    return coll;
}

Akonadi::Collection NntpResource::createCollection(const QModelIndex &index) const
{
    static const QStringList contentTypes = QStringList()
        << KMime::Message::mimeType()
    ;

    const QString name = index.data(NewsgroupsModel::NewsgroupNameRole).toString();
    Collection coll;
    coll.setRemoteId(name);
    coll.setContentMimeTypes(contentTypes);
    coll.setName(name);
    coll.setRights(Collection::ReadOnly);
    EntityDisplayAttribute *edAttr = coll.attribute<EntityDisplayAttribute>(Collection::AddIfMissing);
    edAttr->setDisplayName(index.data(Qt::DisplayRole).toString());
    return coll;
}

AKONADI_RESOURCE_MAIN(NntpResource)
