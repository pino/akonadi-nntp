/*
    Copyright (c) 2008 Volker Krause <vkrause@kde.org>
    Copyright (c) 2017 Pino Toscano <pino@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "settings.h"

#include <kwallet.h>

#include <nntp_debug.h>

using KWallet::Wallet;

class SettingsHelper
{
public:
    SettingsHelper() : q(nullptr) {}
    ~SettingsHelper()
    {
        delete q;
    }
    Settings *q;
};

Q_GLOBAL_STATIC(SettingsHelper, s_globalSettings)

Settings *Settings::self()
{
    if (!s_globalSettings->q) {
        new Settings;
        s_globalSettings->q->load();
    }

    return s_globalSettings->q;
}

Settings::Settings() : SettingsBase()
{
    Q_ASSERT(!s_globalSettings->q);
    s_globalSettings->q = this;
}

void Settings::setIdentifier(const QString &identifier)
{
    mId = identifier;
}

void Settings::setWinId(WId winId)
{
    mWinId = winId;
}

void Settings::cleanup()
{
    Wallet *wallet = Wallet::openWallet(Wallet::NetworkWallet(), mWinId);
    if (wallet && wallet->isOpen()) {
        if (wallet->hasFolder(QStringLiteral("nntp"))) {
            wallet->setFolder(QStringLiteral("nntp"));
            wallet->removeEntry(config()->name());
        }
        delete wallet;
    }
}

void Settings::clearCachedPassword()
{
    mPassword.clear();
}

void Settings::requestPassword()
{
    if (!mPassword.isEmpty()) {
        Q_EMIT passwordRequestCompleted(mPassword, false);
    } else {
        Wallet *wallet = Wallet::openWallet(Wallet::NetworkWallet(), mWinId, Wallet::Asynchronous);
        if (wallet) {
            connect(wallet, &KWallet::Wallet::walletOpened,
                    this, &Settings::onWalletOpened);
        } else {
            QMetaObject::invokeMethod(this, "onWalletOpened", Qt::QueuedConnection, Q_ARG(bool, true));
        }
    }
}

QString Settings::password(bool *userRejected) const
{
    if (userRejected) {
        *userRejected = false;
    }

    if (!mPassword.isEmpty()) {
        return mPassword;
    }
    Wallet *wallet = Wallet::openWallet(Wallet::NetworkWallet(), mWinId);
    if (wallet && wallet->isOpen()) {
        if (wallet->hasFolder(QStringLiteral("nntp"))) {
            wallet->setFolder(QStringLiteral("nntp"));
            wallet->readPassword(config()->name(), mPassword);
        } else {
            wallet->createFolder(QStringLiteral("nntp"));
        }
    } else if (userRejected != nullptr) {
        *userRejected = true;
    }
    delete wallet;
    return mPassword;
}

void Settings::setPassword(const QString &password)
{
    if (password == mPassword) {
        return;
    }

    mPassword = password;
    Wallet *wallet = Wallet::openWallet(Wallet::NetworkWallet(), mWinId);
    if (wallet && wallet->isOpen()) {
        if (!wallet->hasFolder(QStringLiteral("nntp"))) {
            wallet->createFolder(QStringLiteral("nntp"));
        }
        wallet->setFolder(QStringLiteral("nntp"));
        wallet->writePassword(config()->name(), password);
        qCDebug(NNTP_LOG) << "Wallet save: " << wallet->sync();
    }
    delete wallet;
}

void Settings::onWalletOpened(bool success)
{
    if (!success) {
        Q_EMIT passwordRequestCompleted(QString(), true);
    } else {
        Wallet *wallet = qobject_cast<Wallet *>(sender());
        bool passwordNotStoredInWallet = true;
        if (wallet && wallet->hasFolder(QStringLiteral("nntp"))) {
            wallet->setFolder(QStringLiteral("nntp"));
            wallet->readPassword(config()->name(), mPassword);
            passwordNotStoredInWallet = false;
        }

        Q_EMIT passwordRequestCompleted(mPassword, passwordNotStoredInWallet);

        if (wallet) {
            wallet->deleteLater();
        }
    }
}
