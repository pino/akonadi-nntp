/*
    Copyright (c) 2017 Pino Toscano <pino@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "configdialog.h"

#include "newsgroupsmodel.h"
#include "nntputils.h"
#include "settings.h"

#include <resourcesettings.h>

#include <kconfigdialogmanager.h>
#include <klocalizedstring.h>
#include <kmessagebox.h>

#include <QDir>
#include <QGuiApplication>
#include <QIcon>
#include <QNetworkConfigurationManager>
#include <QRegularExpressionValidator>
#include <QStandardPaths>

#if QT_VERSION >= 0x050a00
#include <QSortFilterProxyModel>
#else
#include <krecursivefilterproxymodel.h>
#endif

#if KF5MailTransport_FOUND
#include <mailtransport/transport.h>
#include <mailtransport/servertest.h>
#endif

#include <libetpan/newsnntp.h>

#include <nntp_debug.h>

using namespace MailTransport;
using namespace NntpUtils;

static QString cacheFileName()
{
    const QString dir = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    QDir().mkpath(dir);
    return dir + QStringLiteral("/groups");
}

struct NewsnntpDeleter
{
    static inline void cleanup(newsnntp *nntp)
    {
        newsnntp_free(nntp);
    }
};


NewsgroupsListerThread::NewsgroupsListerThread(QObject *parent)
    : QThread(parent)
    , mPort(0)
    , mEncryptionMode(Settings::EncryptionNone)
    , mAuthenticationRequired(false)
{
}

NewsgroupsListerThread::~NewsgroupsListerThread()
{
}

void NewsgroupsListerThread::setServer(const QString &server, quint16 port, int encryptionMode)
{
    mServer = server;
    mPort = port;
    mEncryptionMode = encryptionMode;
}

void NewsgroupsListerThread::setAuthentication(const QString &userName, const QString &password)
{
    mUserName = userName;
    mPassword = password;
    mAuthenticationRequired = true;
}

void NewsgroupsListerThread::setSubscribedGroups(const QSet<QString> &subscribedGroups)
{
    mSubscribedGroups = subscribedGroups;
}

NewsgroupsModel *NewsgroupsListerThread::takeModel()
{
    return mResultModel.take();
}

QString NewsgroupsListerThread::errorString() const
{
    return mErrorString;
}


void NewsgroupsListerThread::run()
{
    QScopedPointer<newsnntp, NewsnntpDeleter> nntp(newsnntp_new(0, nullptr));
    int ret = mEncryptionMode == Settings::EncryptionSsl
            ? newsnntp_ssl_connect(nntp.data(), mServer.toUtf8().constData(), mPort)
            : newsnntp_socket_connect(nntp.data(), mServer.toUtf8().constData(), mPort);
    qCDebug(NNTP_LOG) << "connect:" << ret;
    if (ret != NEWSNNTP_NO_ERROR) {
        listFailed(i18n("Connection error: %1", etpan_newsnntp_strerror(ret)));
        return;
    }

    if (isInterruptionRequested()) {
        return;
    }

    ret = newsnntp_mode_reader(nntp.data());
    // TODO: not in RFC 977, so we should not abort here
    qCDebug(NNTP_LOG) << "reader:" << ret;
    if (ret != NEWSNNTP_NO_ERROR && ret != NEWSNNTP_ERROR_COMMAND_NOT_UNDERSTOOD && ret != NEWSNNTP_ERROR_COMMAND_NOT_SUPPORTED) {
        listFailed(i18n("Error while setting reader mode: %1", etpan_newsnntp_strerror(ret)));
        return;
    }

    if (isInterruptionRequested()) {
        return;
    }

    if (mAuthenticationRequired) {
        ret = newsnntp_authinfo_username(nntp.data(), mUserName.toUtf8().constData());
        qCDebug(NNTP_LOG) << "authinfo_username:" << ret;
        if (isInterruptionRequested()) {
            return;
        }
        if (ret == NEWSNNTP_WARNING_REQUEST_AUTHORIZATION_PASSWORD) {
            ret = newsnntp_authinfo_password(nntp.data(), mPassword.toUtf8().constData());
            qCDebug(NNTP_LOG) << "authinfo_password:" << ret;
            if (ret != NEWSNNTP_NO_ERROR) {
                listFailed(i18n("Error when setting password: %1", etpan_newsnntp_strerror(ret)));
                return;
            }
        } else if (ret != NEWSNNTP_NO_ERROR) {
            listFailed(i18n("Error when setting username: %1", etpan_newsnntp_strerror(ret)));
            return;
        }
    }

    if (isInterruptionRequested()) {
        return;
    }

    clist *result = nullptr;
    ret = newsnntp_list(nntp.data(), &result);
    qCDebug(NNTP_LOG) << "list:" << ret;
    if (ret != NEWSNNTP_NO_ERROR) {
        listFailed(i18n("Error when listing newsgroups: %1", etpan_newsnntp_strerror(ret)));
        return;
    }

    if (isInterruptionRequested()) {
        return;
    }

    mResultModel.reset(new NewsgroupsModel(NewsgroupsModel::TreeModel, nullptr));
    // move to the application thread, so it can be properly used in GUI
    mResultModel->moveToThread(QCoreApplication::instance()->thread());

    for (clistiter *iter = clist_begin(result); iter != nullptr; iter = clist_next(iter)) {
        const struct newsnntp_group_info *info = static_cast<struct newsnntp_group_info *>(clist_content(iter));

        if (isInterruptionRequested()) {
            newsnntp_list_free(result);
            return;
        }

        const QString name = QString::fromUtf8(info->grp_name);
        const QModelIndex index = mResultModel->addNewsgroup(name);
        if (mSubscribedGroups.contains(name)) {
            mResultModel->setData(index, Qt::Checked, Qt::CheckStateRole);
        }
    }

    newsnntp_list_free(result);
    newsnntp_quit(nntp.data());

    emit listResult(true);
}

void NewsgroupsListerThread::listFailed(const QString &errorString)
{
    mResultModel.reset();
    mErrorString = errorString;
    emit listResult(false);
}


ConfigDialog::ConfigDialog(QWidget *parent)
    : QDialog(parent)
{
    mUi.setupUi(this);

    setWindowIcon(QIcon::fromTheme(QStringLiteral("message-news")));

    mManager = new KConfigDialogManager(this, Settings::self());
    mManager->updateWidgets();
    mUi.kcfg_Server->setValidator(new QRegularExpressionValidator(QRegularExpression(QStringLiteral("[A-Za-z0-9-_:.]*")), mUi.kcfg_Server));
    mUi.kcfg_MaxDownload->setSuffix(ki18np(" article", " articles"));
    mUi.kcfg_IntervalCheckTime->setSuffix(ki18np(" minute", " minutes"));
    mUi.kcfg_IntervalCheckTime->setMinimum(Akonadi::ResourceSettings::self()->minimumCheckInterval());
    connect(mUi.refreshGroupsButton, &QPushButton::clicked, this, &ConfigDialog::refreshGroups);
    connect(mUi.kcfg_Server, &QLineEdit::textChanged, this, &ConfigDialog::slotServerChanged);
    connect(mUi.kcfg_Server, &QLineEdit::textChanged, this, &ConfigDialog::slotTestChanged);
    connect(mUi.kcfg_Server, &QLineEdit::textChanged, this, &ConfigDialog::abortListing);

#if QT_VERSION >= 0x050a00
    mProxy = new QSortFilterProxyModel(mUi.groupsView);
    mProxy->setRecursiveFilteringEnabled(true);
#else
    mProxy = new KRecursiveFilterProxyModel(mUi.groupsView);
#endif
    mProxy->setFilterCaseSensitivity(Qt::CaseInsensitive);
    mGroupsModel = new NewsgroupsModel(NewsgroupsModel::TreeModel, mProxy);
    mProxy->setSourceModel(mGroupsModel);
    mUi.groupsView->setModel(mProxy);
    mUi.kfilterproxysearchline->setProxy(mProxy);
    mUi.groupsView->sortByColumn(0, Qt::AscendingOrder);

    mUi.groupsMessage->hide();
    mUi.groupsProgress->hide();

    mUi.safeNntpGroup->setId(mUi.encryptionNone, Settings::EncryptionNone);
    mUi.safeNntpGroup->setId(mUi.encryptionSsl, Settings::EncryptionSsl);
    mUi.safeNntpGroup->setId(mUi.encryptionTls, Settings::EncryptionTls);
    connect(mUi.safeNntpGroup, static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::buttonClicked), this, &ConfigDialog::slotEncryptionRadioChanged);
#if KF5MailTransport_FOUND
    connect(mUi.checkCapabilities, &QPushButton::clicked, this, &ConfigDialog::slotStartTest);
#else
    mUi.checkCapabilitiesStack->hide();
#endif

    QPushButton *okButton = mUi.buttonBox->button(QDialogButtonBox::Ok);
    okButton->setDefault(true);
    okButton->setShortcut(Qt::CTRL | Qt::Key_Return);
    connect(mUi.buttonBox, &QDialogButtonBox::rejected, this, &ConfigDialog::reject);
    connect(okButton, &QPushButton::clicked, this, &ConfigDialog::applySettings);

    QNetworkConfigurationManager *networkConfigMgr = new QNetworkConfigurationManager(this);
    connect(networkConfigMgr, &QNetworkConfigurationManager::onlineStateChanged,
            this, &ConfigDialog::slotOnlineChanged);

    readSettings();
    slotTestChanged();
    if (mGroupsModel->fillFromFile(cacheFileName())) {
        mUi.groupsView->expandAll();
        foreach (const QString &s, mSubscribedGroups) {
            const QModelIndex index = mGroupsModel->findNewsgroup(s);
            if (!index.isValid()) {
                continue;
            }
            mGroupsModel->setData(index, Qt::Checked, Qt::CheckStateRole);
        }
    }
}

ConfigDialog::~ConfigDialog()
{
    abortListing();
}

void ConfigDialog::applySettings()
{
    Settings::self()->setEncryption(mUi.safeNntpGroup->checkedId());
    if (mUi.kcfg_RequiresAuthentication->isChecked() && mUi.password->isEnabled()) {
        Settings::self()->setPassword(mUi.password->text());
    }
    Settings::self()->setSubscriptions(mGroupsModel->checkedNewsgroups());
    mManager->updateSettings();
    Settings::self()->save();

    mGroupsModel->saveToFile(cacheFileName());

    accept();
}

void ConfigDialog::refreshGroups()
{
    abortListing();

    mSubscribedGroups = QSet<QString>::fromList(mGroupsModel->checkedNewsgroups());
    mGroupsModel->clear();
    mUi.groupsMessage->hide();

    mThread.reset(new NewsgroupsListerThread());
    mThread->setServer(mUi.kcfg_Server->text(), mUi.kcfg_Port->value(), mUi.safeNntpGroup->checkedId());
    mThread->setSubscribedGroups(mSubscribedGroups);
    if (mUi.kcfg_RequiresAuthentication->isChecked()) {
        mThread->setAuthentication(mUi.kcfg_UserName->text(),
                                   mUi.password->isEnabled() ? mUi.password->text() : QString());
    }
    connect(mThread.data(), &NewsgroupsListerThread::listResult,
            this, &ConfigDialog::slotListResult);

    mUi.groupsProgress->setRange(0, 0);
    mUi.groupsProgress->show();

    mThread->start();
}

void ConfigDialog::slotServerChanged()
{
    QGuiApplication::restoreOverrideCursor();

    mUi.encryptionNone->setEnabled(true);
    mUi.encryptionSsl->setEnabled(true);
    mUi.encryptionTls->setEnabled(true);
}

void ConfigDialog::slotTestChanged()
{
#if KF5MailTransport_FOUND
    mServerTest.reset();
#endif

    mUi.buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    mUi.checkCapabilitiesStack->setCurrentIndex(0);
    mUi.checkCapabilities->setEnabled(true);
    mUi.encryptionGroup->setEnabled(true);
}

void ConfigDialog::slotEncryptionRadioChanged()
{
    switch (mUi.safeNntpGroup->checkedId()) {
    case Settings::EncryptionNone:
    case Settings::EncryptionTls:
        mUi.kcfg_Port->setValue(DEFAULT_NNTP_PORT);
        break;
    case Settings::EncryptionSsl:
        mUi.kcfg_Port->setValue(DEFAULT_NNTPS_PORT);
        break;
    default:
        qFatal("Shouldn't happen");
    }
}

void ConfigDialog::slotStartTest()
{
#if KF5MailTransport_FOUND
    mUi.checkCapabilities->setEnabled(false);
    mUi.encryptionGroup->setEnabled(false);

    mServerTest.reset(new ServerTest(this));
    QGuiApplication::setOverrideCursor(Qt::BusyCursor);

    const QString server = mUi.kcfg_Server->text();
    const int port = mUi.kcfg_Port->value();
    qCDebug(NNTP_LOG) << "server:" << server << "port:" << port;

    mServerTest->setServer(server);

    if (port != DEFAULT_NNTP_PORT && port != DEFAULT_NNTPS_PORT) {
        mServerTest->setPort(Transport::EnumEncryption::None, port);
        mServerTest->setPort(Transport::EnumEncryption::SSL, port);
    }

    mServerTest->setProtocol(QStringLiteral("nntp"));
    mServerTest->setProgressBar(mUi.checkCapabilitiesProgress);
    connect(mServerTest.data(), &MailTransport::ServerTest::finished, this, &ConfigDialog::slotTestFinished);
    mUi.buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    mUi.checkCapabilitiesStack->setCurrentIndex(1);
    mServerTest->start();
#endif
}

void ConfigDialog::slotTestFinished(const QVector<int> &testResult)
{
#if KF5MailTransport_FOUND
    qCDebug(NNTP_LOG) << testResult;

    QGuiApplication::restoreOverrideCursor();
    mUi.buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    mUi.checkCapabilitiesStack->setCurrentIndex(0);

    if (!mServerTest->isNormalPossible() && !mServerTest->isSecurePossible()) {
        KMessageBox::sorry(this, i18n("Unable to connect to the server, please verify the server address."));
    }

    mUi.encryptionSsl->setEnabled(testResult.contains(Transport::EnumEncryption::SSL));
    mUi.encryptionTls->setEnabled(testResult.contains(Transport::EnumEncryption::TLS));
    mUi.encryptionNone->setEnabled(testResult.contains(Transport::EnumEncryption::None));

    if (testResult.contains(Transport::EnumEncryption::TLS)) {
        mUi.encryptionTls->setChecked(true);
    } else if (testResult.contains(Transport::EnumEncryption::SSL)) {
        mUi.encryptionSsl->setChecked(true);
    } else if (testResult.contains(Transport::EnumEncryption::None)) {
        mUi.encryptionNone->setChecked(true);
    }

    mUi.checkCapabilities->setEnabled(true);
    mUi.encryptionGroup->setEnabled(true);
    slotEncryptionRadioChanged();
#else
    Q_UNUSED(testResult);
#endif
}

void ConfigDialog::slotOnlineChanged(bool online)
{
    if (!online) {
        if (!mUi.checkCapabilities->isEnabled()) {
            slotTestChanged();
        }
        abortListing();
    }
}

void ConfigDialog::slotListResult(bool succeeded)
{
    if (succeeded) {
        const QScopedPointer<QAbstractItemModel> oldModel(mProxy->sourceModel());
        mGroupsModel = mThread->takeModel();
        mGroupsModel->setParent(mProxy);
        mProxy->setSourceModel(mGroupsModel);
        mUi.groupsView->expandAll();
    } else {
        mUi.groupsMessage->setText(mThread->errorString());
        mUi.groupsMessage->show();
    }
    mUi.groupsProgress->setRange(0, 100); // avoid it spinning
    mUi.groupsProgress->hide();
    mThread->wait();
    mThread.reset();
}

void ConfigDialog::abortListing()
{
    if (!mThread) {
        return;
    }

    disconnect(mThread.data(), nullptr, this, nullptr);
    mThread->requestInterruption();
    mUi.groupsProgress->setRange(0, 100); // avoid it spinning
    mUi.groupsProgress->hide();
    mThread->wait();
    mThread.reset();
}

void ConfigDialog::readSettings()
{
    if (Settings::self()->requiresAuthentication()) {
        bool rejected = false;
        const QString password = Settings::self()->password(&rejected);
        if (rejected) {
            mUi.password->setEnabled(false);
            KMessageBox::information(nullptr, i18n("Could not access KWallet. "
                                     "If you want to store the password permanently then you have to "
                                     "activate it. If you do not "
                                     "want to use KWallet, check the box below, but note that you will be "
                                     "prompted for your password when needed."),
                                     i18nc("@title:window", "Do not use KWallet"), QStringLiteral("warning_kwallet_disabled"));
        } else {
            mUi.password->setText(password);
        }
    }
    QAbstractButton *encryptionButton = mUi.safeNntpGroup->button(Settings::self()->encryption());
    if (encryptionButton) {
        encryptionButton->setChecked(true);
    }

    mSubscribedGroups = QSet<QString>::fromList(Settings::self()->subscriptions());
}
