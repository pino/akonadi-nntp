/*
    Copyright (c) 2017 Pino Toscano <pino@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#ifndef NEWSGROUPSMODEL_H
#define NEWSGROUPSMODEL_H

#include <QAbstractItemModel>

class NewsgroupsModelPrivate;

class NewsgroupsModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    enum Type { TreeModel, FlatModel };
    enum FlattenDisplayType { NormalFlattenDisplay, ShortenedFlattenDisplay };
    enum { CollectionRole = Qt::UserRole + 100, NewsgroupNameRole };

    explicit NewsgroupsModel(Type type, QObject *parent = nullptr);
    ~NewsgroupsModel();

    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) Q_DECL_OVERRIDE;

    Type type() const;
    void setFlattenDisplayType(FlattenDisplayType displayType);
    QModelIndex addNewsgroup(const QString &name);
    void clear();
    QModelIndex findNewsgroup(const QString &name) const;
    QStringList checkedNewsgroups() const;
    bool fillFromFile(const QString &file);
    bool saveToFile(const QString &file) const;

private:
    Q_DISABLE_COPY(NewsgroupsModel)
    friend class NewsgroupsModelPrivate;
    NewsgroupsModelPrivate *const d;
};

#endif
