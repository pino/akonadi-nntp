/*
    Copyright (c) 2017 Pino Toscano <pino@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H

#include <QDialog>
#include <QScopedPointer>
#include <QSet>
#include <QThread>

#include <ui_configdialog.h>
#include <config-akonadi-nntp.h>

class NewsgroupsModel;
class KConfigDialogManager;
class QSortFilterProxyModel;
namespace MailTransport
{
class ServerTest;
}

class NewsgroupsListerThread : public QThread
{
    Q_OBJECT

public:
    explicit NewsgroupsListerThread(QObject *parent = nullptr);
    ~NewsgroupsListerThread();

    void setServer(const QString &server, quint16 port, int encryptionMode);
    void setAuthentication(const QString &userName, const QString &password);
    void setSubscribedGroups(const QSet<QString> &subscribedGroups);

    NewsgroupsModel *takeModel();
    QString errorString() const;

Q_SIGNALS:
    void listResult(bool succeeded);

protected:
    void run() Q_DECL_OVERRIDE;

private:
    void listFailed(const QString &errorString);

private:
    Q_DISABLE_COPY(NewsgroupsListerThread)
    QString mServer;
    quint16 mPort;
    int mEncryptionMode;
    bool mAuthenticationRequired;
    QString mUserName;
    QString mPassword;
    QSet<QString> mSubscribedGroups;
    QScopedPointer<NewsgroupsModel> mResultModel;
    QString mErrorString;
};

class ConfigDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConfigDialog(QWidget *parent = nullptr);
    ~ConfigDialog();

private Q_SLOTS:
    void applySettings();
    void refreshGroups();
    void slotServerChanged();
    void slotTestChanged();
    void slotEncryptionRadioChanged();
    void slotStartTest();
    void slotTestFinished(const QVector<int> &testResult);
    void slotOnlineChanged(bool online);
    void slotListResult(bool succeeded);
    void abortListing();

private:
    void readSettings();

private:
    Q_DISABLE_COPY(ConfigDialog)
    Ui::ConfigDialog mUi;
    KConfigDialogManager *mManager;
    QSortFilterProxyModel *mProxy;
    NewsgroupsModel *mGroupsModel;
    QSet<QString> mSubscribedGroups;
    QScopedPointer<NewsgroupsListerThread> mThread;
#if KF5MailTransport_FOUND
    QScopedPointer<MailTransport::ServerTest> mServerTest;
#endif
};

#endif
