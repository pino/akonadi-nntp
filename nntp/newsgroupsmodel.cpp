/*
    Copyright (c) 2017 Pino Toscano <pino@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "newsgroupsmodel.h"

#include <QFile>
#include <QTextStream>
#include <QVector>

#include <functional>

static const QChar periodChar = QLatin1Char('.');

/**
 * Extract the substring for the first @p count sections of @p string;
 * if there are less sections than requested, the whole string is returned:
 * - "foo.bar.example", 2 -> "foo.bar"
 * - "foo.bar.example", 5 -> "foo.bar.example"
 */
static QStringRef extractFirstNSections(const QStringRef &string, int count)
{
    int index = 0;
    for (int i = 0; i < count; ++i) {
        const int newIndex = string.indexOf(periodChar, index);
        if (newIndex < 0) {
            return string;
        }
        index = newIndex + 1;
    }
    return string.left(index - 1);
}

/**
 * Shorten a newsgroup path to leave only the initial character for
 * all the sections but the last one:
* - "foo.bar.example" -> "f.b.example"
 */
static QString shortenedTitle(const QString &name)
{
    QString ret;
    const QVector<QStringRef> path = name.splitRef(periodChar);
    Q_ASSERT(!path.isEmpty());
    const int actualPathItems = path.count() - 1;
    ret.reserve(actualPathItems * 2 + path.last().length());
    for (int i = 0; i < actualPathItems; ++i) {
        ret.append(path.at(i).at(0));
        ret.append(periodChar);
    }
    ret.append(path.last());
    return ret;
}

struct NGItem
{
    NGItem(NGItem *parent = nullptr);
    ~NGItem();

    NGItem* find(const QStringRef &name) const;

    QString text;
    QString newsgroup;
    QVector<NGItem *> children;
    NGItem *parent;
    QVariant collection;
    bool checkable : 1;
    bool checked : 1;
};


class NewsgroupsModelPrivate
{
public:
    NewsgroupsModelPrivate(NewsgroupsModel *qq);
    ~NewsgroupsModelPrivate();

    QModelIndex indexForItem(NGItem *item) const;
    QModelIndex addTree(const QString &name);
    QModelIndex addFlat(const QString &name);
    void iterate(std::function<void(const NGItem *)> fn) const;

    NewsgroupsModel *q;
    NGItem *root;
    NewsgroupsModel::Type type : 2;
    NewsgroupsModel::FlattenDisplayType displayType : 2;
};


NGItem::NGItem(NGItem *p)
    : parent(p), checkable(false), checked(false)
{
    if (parent) {
        parent->children.append(this);
    }
}

NGItem::~NGItem()
{
    qDeleteAll(children);
}

NGItem* NGItem::find(const QStringRef &name) const
{
    foreach (NGItem *child, children) {
        if (child->newsgroup == name) {
            return child;
        }
    }
    return nullptr;
}


NewsgroupsModelPrivate::NewsgroupsModelPrivate(NewsgroupsModel *qq)
    : q(qq), root(new NGItem)
    , displayType(NewsgroupsModel::NormalFlattenDisplay)
{
}

NewsgroupsModelPrivate::~NewsgroupsModelPrivate()
{
    delete root;
}

QModelIndex NewsgroupsModelPrivate::indexForItem(NGItem *item) const
{
    if (item->parent) {
        const int id = item->parent->children.indexOf(item);
        if (id >= 0 && id < item->parent->children.count()) {
           return q->createIndex(id, 0, item);
        }
    }
    return QModelIndex();
}

QModelIndex NewsgroupsModelPrivate::addTree(const QString &name)
{
    const QVector<QStringRef> path = name.splitRef(periodChar);
    Q_ASSERT(!path.isEmpty());
    const QStringRef nameRef(&name);
    const int actualPathItems = path.count() - 1;
    NGItem *item = root;
    for (int i = 0; i < actualPathItems; ++i) {
        const QStringRef text = extractFirstNSections(nameRef, i + 1);
        NGItem *newItem = item->find(text);
        if (!newItem) {
            const int childCount = item->children.count();
            const QModelIndex index = indexForItem(item);
            q->beginInsertRows(index, childCount, childCount);
            newItem = new NGItem(item);
            newItem->newsgroup = newItem->text = text.toString();
            q->endInsertRows();
        }
        item = newItem;
    }
    Q_ASSERT(item);
    NGItem *newItem = item->find(nameRef);
    if (!newItem) {
        const int childCount = item->children.count();
        const QModelIndex index = indexForItem(item);
        q->beginInsertRows(index, childCount, childCount);
        newItem = new NGItem(item);
        newItem->newsgroup = newItem->text = name;
        newItem->checkable = true;
        q->endInsertRows();
    } else if (!newItem->checkable) {
        const QModelIndex index = indexForItem(newItem);
        newItem->checkable = true;
        emit q->dataChanged(index, index);
        return index;
    }
    return indexForItem(newItem);
}

QModelIndex NewsgroupsModelPrivate::addFlat(const QString &name)
{
    const QStringRef nameRef(&name);
    NGItem *newItem = root->find(nameRef);
    if (!newItem) {
        const int childCount = root->children.count();
        q->beginInsertRows(QModelIndex(), childCount, childCount);
        newItem = new NGItem(root);
        newItem->text = name;
        newItem->newsgroup = name;
        newItem->checkable = true;
        q->endInsertRows();
    } else if (!newItem->checkable) {
        const QModelIndex index = indexForItem(newItem);
        newItem->checkable = true;
        emit q->dataChanged(index, index);
        return index;
    }
    return indexForItem(newItem);
}

void NewsgroupsModelPrivate::iterate(std::function<void(const NGItem *)> fn) const
{
    QVector<NGItem *> queue;
    queue.reserve(20);
    queue.push_back(root);
    while (!queue.isEmpty()) {
        const NGItem *item = queue.takeFirst();
        const int count = item->children.count();
        for (int i = 0; i < count; ++i) {
            queue.push_back(item->children.at(i));
        }
        fn(item);
    }
}


NewsgroupsModel::NewsgroupsModel(Type type, QObject *parent)
    : QAbstractItemModel(parent)
    , d(new NewsgroupsModelPrivate(this))
{
    d->type = type;
}

NewsgroupsModel::~NewsgroupsModel()
{
    delete d;
}

int NewsgroupsModel::columnCount(const QModelIndex &) const
{
    return 1;
}

QVariant NewsgroupsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    const NGItem *item = static_cast<NGItem *>(index.internalPointer());
    switch (role) {
    case Qt::DisplayRole:
        if (d->type == FlatModel && d->displayType == ShortenedFlattenDisplay) {
            return shortenedTitle(item->text);
        }
        return item->text;
    case Qt::ToolTipRole:
        return item->text;
    case NewsgroupNameRole:
        return item->newsgroup;
    case Qt::CheckStateRole:
        if (!item->checkable) {
            return QVariant();
        }
        return item->checked ? Qt::Checked : Qt::Unchecked;
    case CollectionRole:
        return item->collection;
    }
    return QVariant();
}

Qt::ItemFlags NewsgroupsModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags baseFlags = QAbstractItemModel::flags(index);
    if (index.isValid()) {
        const NGItem *item = static_cast<NGItem *>(index.internalPointer());
        if (item->checkable) {
            baseFlags |= Qt::ItemIsUserCheckable;
        }
    }
    return baseFlags;
}

QModelIndex NewsgroupsModel::index(int row, int column, const QModelIndex &parent) const
{
    if (row < 0 || column != 0) {
        return QModelIndex();
    }

    const NGItem *item = parent.isValid() ? static_cast<NGItem *>(parent.internalPointer()) : d->root;
    if (row < item->children.count()) {
        return createIndex(row, column, item->children.at(row));
    }

    return QModelIndex();
}

QModelIndex NewsgroupsModel::parent(const QModelIndex &index) const
{
    if (!index.isValid()) {
        return QModelIndex();
    }

    const NGItem *item = static_cast<NGItem *>(index.internalPointer());
    return d->indexForItem(item->parent);
}

int NewsgroupsModel::rowCount(const QModelIndex &parent) const
{
    const NGItem *item = parent.isValid() ? static_cast<NGItem *>(parent.internalPointer()) : d->root;
    return item->children.count();
}

bool NewsgroupsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid()) {
        return false;
    }

    NGItem *item = static_cast<NGItem *>(index.internalPointer());

    switch (role) {
    case Qt::CheckStateRole:
        if (!item->checkable) {
            return false;
        }
        item->checked = static_cast<Qt::CheckState>(value.toInt()) == Qt::Checked;
        emit dataChanged(index, index);
        return true;
    case CollectionRole:
        item->collection = value;
        emit dataChanged(index, index);
        return true;
    }

    return false;
}

NewsgroupsModel::Type NewsgroupsModel::type() const
{
    return d->type;
}

void NewsgroupsModel::setFlattenDisplayType(NewsgroupsModel::FlattenDisplayType displayType)
{
    d->displayType = displayType;
}

QModelIndex NewsgroupsModel::addNewsgroup(const QString &name)
{
    switch (d->type) {
    case TreeModel:
        return d->addTree(name);
    case FlatModel:
        return d->addFlat(name);
    }
    return QModelIndex();
}

void NewsgroupsModel::clear()
{
    beginResetModel();
    delete d->root;
    d->root = new NGItem();
    endResetModel();
}

QModelIndex NewsgroupsModel::findNewsgroup(const QString &name) const
{
    const QStringRef ref(&name);
    switch (d->type) {
    case TreeModel: {
        NGItem *item = d->root;
        const int count = name.count(periodChar) + 1;
        for (int i = 0; i < count; ++i) {
            NGItem *child = item->find(extractFirstNSections(ref, i+1));
            if (!child) {
                return QModelIndex();
            }
            item = child;
        }
        Q_ASSERT(item);
        return d->indexForItem(item);
    }
    case FlatModel: {
        NGItem *child = d->root->find(ref);
        return child ? d->indexForItem(child) : QModelIndex();
    }
    }
    return QModelIndex();
}

QStringList NewsgroupsModel::checkedNewsgroups() const
{
    QStringList list;
    d->iterate(
        [&list](const NGItem *item) {
            if (item->checkable && item->checked) {
                list.append(item->newsgroup);
            }
        }
    );
    return list;
}

bool NewsgroupsModel::fillFromFile(const QString &file)
{
    QFile f(file);
    if (!f.exists() || !f.open(QIODevice::ReadOnly)) {
        return false;
    }

    QTextStream stream(&f);
    clear();
    QString s;
    while (stream.readLineInto(&s)) {
        addNewsgroup(s);
    }
    return true;
}

bool NewsgroupsModel::saveToFile(const QString &file) const
{
    QFile f(file);
    if (!f.open(QIODevice::WriteOnly)) {
        return false;
    }

    QTextStream stream(&f);
    d->iterate(
        [&stream](const NGItem *item) {
            if (item->checkable) {
                stream << item->newsgroup << endl;
            }
        }
    );
    return true;
}
